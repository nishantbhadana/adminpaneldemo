import React from 'react';
import classes from './OrderSummary.css';

const OrderSummary = (props) => {

    return(
        <div className={classes.Container}>
            <div className={classes.Details} >
                <div className={classes.Title} >Order ID : 123456789</div>
                <p>Customer ID : 258654</p>
                <p> Name : Nishant Bhadana</p>
                <p>Email : nishantbhadana@gmail</p>
                <p> Contact: +91-9999705551</p >
                <p> Address : #1744, Village PAli, Faridabad, 121004</p >
            </div><br/>
            <div>
                 <div className={classes.Details1}>
                    <div className={classes.Title} >Order Details</div>
                    <p>ABC              Cost : 12$</p>
                    <p>DEF              Cost : 22$</p>
                    <p>GHI              Cost : 19$</p>
                    <p>JKL              Cost : 85$</p>
                    <p>MNO              Cost : 120$</p>
                </div>
            </div>
        </div>
    )

}

export default OrderSummary ;