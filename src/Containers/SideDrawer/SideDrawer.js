import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import classNames from 'classnames';
import Divider from 'material-ui/Divider';
import Drawer from 'material-ui/Drawer';
import AppBar from 'material-ui/AppBar';
import Avatar from 'material-ui/Avatar';
import deepOrange from 'material-ui/colors/deepOrange';
import deepPurple from 'material-ui/colors/deepPurple';
import Toolbar from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import { Home, Assignment, Assessment, settingsPower } from 'material-ui-icons';
import MenuIcon from 'material-ui-icons/Menu';
import ChevronLeftIcon from 'material-ui-icons/ChevronLeft';
import ChevronRightIcon from 'material-ui-icons/ChevronRight';

import Home1 from '../Home/Home';
import OrderStatus from '../OrderStatus/OrderStatus';
import OrderSummary from '../OrderSummary/OrderSummary';
import Details from '../OrderDetails/OrderDetails';
import SignOut from '../../Components/SignOut/SignOut';

import BackImg from '../../Assets/abstract.jpg'
import Classes from './SideDrawer.css';

import {NavLink, Switch , Route } from 'react-router-dom';


const drawerWidth = 240;

const styles = theme => ({
  root: {
    marginTop: 0 ,
    width: '100%',
    height: '100%',
    //marginTop: theme.spacing.unit * 3,
    zIndex: 1,
    overflow: 'hidden',
  },
  avatar: {
     margin: 500,
     

  },
  orangeAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: deepOrange[500],
  },
  purpleAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: deepPurple[500],
  },
  appFrame: {
    position: 'relative',
    display: 'flex',
    width: '100%',
    height: '100%',
  },
  appBar: {
    position: 'absolute',
        transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  'appBarShift-left': {
    marginLeft: drawerWidth,
  },
  
  menuButton: {
    marginLeft: 12,
    marginRight: 20,
  },
  hide: {
    display: 'none',
  },
  drawerPaper: {
    position: 'relative',
    height: '100%',
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    width: '100%',
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    height: 'calc(100% - 56px)',
    marginTop: 56,
    [theme.breakpoints.up('sm')]: {
      height: 'calc(100% - 64px)',
      marginTop: 64,
    },
  },
  'content-left': {
    marginLeft: -drawerWidth,
  },
  contentShift: {
    marginLeft: 0,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
     
    }),
  },

});

class PersistentDrawer extends Component {
  state = {
    open: false,
    anchor: 'left',
  };

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };


  render() {
    const { classes, theme } = this.props;
    const { anchor, open } = this.state;

    const drawer = (
      <Drawer
        variant="persistent"
        classes={{
          paper: classes.drawerPaper,
        }}
        anchor={anchor}
        open={open}
      >
        <div className={classes.drawerInner}>

          <div className={Classes.Head} onClick={this.handleDrawerClose}>
            POST AN IMAGE HERE  
          </div>

          <nav className={Classes.menu1}>

            <div className={Classes.img}> </div>
            <div className={Classes.menu} >
              <ul onClick={this.handleDrawerClose} >
                <NavLink
                  to="/"
                  exact
                  activeClassName="my-active"
                > <Home className={Classes.Icon} />  Home</NavLink><hr />
                <NavLink to="/order-status"
                ><Assignment className={Classes.Icon} />  Pending</NavLink><hr />
                <NavLink to="/order-details"
                ><Assessment className={Classes.Icon} />  Summary</NavLink><hr />
                <NavLink to="/log-out"
                ><settingsPower className={Classes.Icon} />  Sign Out</NavLink>
              </ul>
            </div>
           
           </nav>
          </div>
      </Drawer>
    );


    return (
      <div className={classes.root}>
     
        <div className={classes.appFrame}>
          <AppBar
          
            className={classNames(classes.appBar, {
              [classes.appBarShift]: open,
              [classes[`appBarShift-left`]]: open,
            })}
          >
            <Toolbar disableGutters={!open}>
              <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={this.handleDrawerOpen}
                className={classNames(classes.menuButton, open && classes.hide)}
              >
                <MenuIcon />
              </IconButton>
              <div className={Classes.Redirect} >
                <Home className={Classes.Icon}/><NavLink to="/">Dashboard</NavLink>
              </div>
              
              
            </Toolbar>
          </AppBar>
          {drawer}
          <main
            className={classNames(classes.content, classes[`content-left`], {
              [classes.contentShift]: open,
              [classes[`contentShift`]]: open,
            })}
          >

            <Switch>
              <Route path="/" exact component={Home1} />
              <Route path="/order-status" component={OrderStatus} />
              <Route path="/order-details" component={Details} />
              <Route path="/order-summary" component={OrderSummary} />
              <Route path="/log-out" component={SignOut} />
              
              <Route render={() => <h1>Not found</h1>} />
            </Switch>

           
          </main>
         
       
        </div>
      </div>
    );
  }
}

PersistentDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(PersistentDrawer);