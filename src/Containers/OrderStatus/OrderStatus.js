import React , {Component} from 'react';
import classes from './OrderStatus.css';
import Post from '../../Components/Post/Post';
import axios from 'axios';

class OrderStatus extends Component {


    state = {
        posts: []
    }

    componentDidMount() {
        console.log(this.props);
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then(response => {
                const posts = response.data;
                const updatedPosts = posts.map(post => {
                    return {
                        ...post,
                    }
                });
                this.setState({ posts: updatedPosts });
                console.log( response );
            })
            .catch(error => {
                console.log(error);
            });
    }

    render(){

        let posts = <p style={{ textAlign: 'center' }}>Something went wrong!</p>;
        if (!this.state.error) {
            posts = this.state.posts.map(post => {
                return (
                    <div className={classes.list} >
                        <ul>
                            <li>
                                <Post
                                    key={post.id}
                                    title={post.title}
                                    author={post.author}
                                />
                            </li>
                        </ul>
                    </div>
                   
                );
            });
        }


        return(
        
            <div className={classes.Manage} >
            
                    <div className={classes.Details} >
                        <div className={classes.Title} >
                            UNCONFIRMED ORDERS
                        </div>
                        <div className={classes.listData}>
                           {posts}   
                        </div>                              
                    </div>

                    <div className={classes.Details} >
                        <div className={classes.Title} >
                            CONFIRMED ORDERS
                    </div>
                        <div className={classes.listData}>
                             {posts}
                        </div> 
                    </div>

                    <div className={classes.Details} >
                        <div className={classes.Title} >
                            OUT FOR DELIVERY
                    </div>
                        <div className={classes.listData}>
                             {posts}
                        </div> 
                    </div>
            </div>

        )
    }
}

export default OrderStatus ;