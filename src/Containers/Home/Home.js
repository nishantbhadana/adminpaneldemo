import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import classes from './Home.css';
import Graph from '../../Components/Graph/Graph';
import Table from '../../Components/Table/Table';
import Card from '../../Components/Card/Card';
import Graph2 from '../../Components/Graph/Graph2';

class Home extends Component {



    render() {

        return (
            <div>

                <div className={classes.Manage}>
                    <Link to="/order-status"><Card title="Unconfirmed Orders" Count="8" /></Link>
                    <Link to="/order-status"><Card title="Confirmed Orders" Count="8" /></Link>
                    <Link to="/order-status"><Card title="Out For Delivery" Count="8" /></Link>
                    <Link to="/order-details"><Card title="Total Orders" Count="8" /></Link>
                </div>

                <div className={classes.Mdraw}>

                    <div className={classes.Graph}>
                        <Graph />
                    </div>
                    <div className={classes.Graph}>
                        <Graph2 />
                    </div>

                </div>


                <div className={classes.Table}>
                    <Table />
                </div>


            </div>

        )
    }
}

export default Home;