import React from 'react';
import Classes from './Card.css'


const card = (props) => {
    return (
        <div>

            {/* <CardContent>
                    <Typography className={classes.title}>{props.title}</Typography>
                    <Typography className={classes.pos}>{props.Count}</Typography>
                </CardContent> */}
            <div className={Classes.Details} >
                <div className={Classes.Title} >
                    {props.title}
                </div>
                <div className={Classes.Data} >
                    {props.Count}
                </div>
               
            </div>

        </div>
    )
}

export default card;