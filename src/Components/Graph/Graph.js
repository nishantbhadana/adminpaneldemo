import React , {Component} from "react";
import { Line } from "react-chartjs-2";


class Graph extends Component {

    state = {

       sData: {
                labels: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
                datasets: [
                    {
                        label: "Sales",
                        data : [ 100, 200, 150, 125, 95, 175, 50 ],
                       
                    }
                ],
                backgroundColor : ['rgba(244, 67, 54,1.0)', 'rgba(233, 30, 99,1.0)', 'rgba(156, 39, 176,1.0)', 'rgba(103, 58, 183,1.0)', 'rgba(139, 195, 74,1.0)', 'rgba(121, 85, 72,1.0)','rgba(255, 87, 34,1.0)'
                ]
        }
    }

     

    

    render (){
        return(
            <div>
                <Line

                    data={this.state.sData}
                    width={10}
                    height={350}
                    options={{
                        maintainAspectRatio: false
                    }}
                />
            </div>
        )
    }
}

export default Graph ;