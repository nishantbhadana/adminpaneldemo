import React from 'react';
import Classes from './Post.css';


const post = (props) => (
    <article className={Classes.Data}>
        <h1>{props.title}</h1>
        <div className="Info">
            <div className="Author">{props.author}</div>
        </div>
    </article>
);

export default post;