import React, { Component } from 'react';
import SideDrawer from './Containers/SideDrawer/SideDrawer';
import { BrowserRouter } from 'react-router-dom'

class App extends Component {
  render() {
    return (
      <BrowserRouter>
           <SideDrawer />
      </BrowserRouter>
     
    );
  }
}

export default App;
